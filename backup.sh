#!/bin/bash

function create-backup() {
    # Check arguments
    local paths=()
    while [ $# -gt 0 ]
    do
        if [[ $1 == "-h" || $1 == "--help" ]]
        then
            local program_name=${FUNCNAME[0]}
            echo 'Create a local backup from several files or folder.'
            echo ''
            echo "Usage: ${program_name} [<file>]*"
            return 0
        else
            paths+=($1)
        fi
        shift
    done

    # Create a temporary working directory
    local working_dir=$(mktemp -d)

    # Process each files
    for path in "${paths[@]}"
    do
        if [[ -d ${path} ]]
        then
            # Compress folder
            echo "Compressing folder \"${path}\"..."
            local folder_name=$(basename ${path})
            local archive="${folder_name}.tar"
            tar -cf ${archive} -C ${path} .
            mv ${archive} ${working_dir}
        elif [[ -f ${path} ]]
        then
            # Copy file
            echo "Copying file \"${path}\"..."
            cp ${path} ${working_dir}
        else
            echo "Invalid path \"${path}\" ignored"
        fi
    done

    local date=$(date +'%Y-%m-%d')
    local backup="${date}.tar"
    local destination=${HOME}

    BACKUP_PATH="${destination}/${backup}"

    # Create backup of the working directory
    echo 'Creating backup...'
    tar -cf ${backup} -C ${working_dir} .
    mv ${backup} ${destination}
    echo "Backup created at ${BACKUP_PATH}"

    # Cleanup
    rm -rf ${working_dir}
}

function retrieve-access-token () {
    # Check arguments
    while [ $# -gt 0 ]
    do
        if [[ $1 == "-h" || $1 == "--help" ]]
        then
            local program_name=${FUNCNAME[0]}
            echo 'Retrieve an access token to access the Google Drive API.'
            echo ''
            echo "Usage: ${program_name} [options...] [<file>]*"
            echo ''
            echo ' -c, --config <file>              Configuration file containing Google Drive credentials'
            return 0
        elif [[ $1 == '-c' || $1 == '--config' ]]
        then
            local config=$2
            shift
        fi
        shift
    done

    # Load configuration
    if [[ ! -z "${config}" ]]
    then
        . ${config}
    fi
    if [[ -z "${GOOGLE_DRIVE_CLIENT_ID}" ]]
    then
        echo "GOOGLE_DRIVE_CLIENT_ID not found"
        return 1
    fi
    if [[ -z "${GOOGLE_DRIVE_CLIENT_SECRET}" ]]
    then
        echo "GOOGLE_DRIVE_CLIENT_SECRET not found"
        return 1
    fi

    # Get authorization code
    local redirect_uri='urn:ietf:wg:oauth:2.0:oob'
    local scope='https://www.googleapis.com/auth/drive.file'
    local response_type='code'
    local link="https://accounts.google.com/o/oauth2/v2/auth?client_id=${GOOGLE_DRIVE_CLIENT_ID}&redirect_uri=${redirect_uri}&scope=${scope}&response_type=${response_type}"
    read -s -p "Please open this link ${link} and enter the authorization code to access Google Drive: " code
    echo -e '\n'

    # Get access token
    local grant_type='authorization_code'
    access_token=$(curl -s https://oauth2.googleapis.com/token \
        --request POST \
        --data "client_id=${GOOGLE_DRIVE_CLIENT_ID}&client_secret=${GOOGLE_DRIVE_CLIENT_SECRET}&redirect_uri=${redirect_uri}&code=${code}&grant_type=${grant_type}" \
    | jq '.access_token')
}

function upload-files() {
    # Check arguments
    local files=()
    while [ $# -gt 0 ]
    do
        if [[ $1 == "-h" || $1 == "--help" ]]
        then
            local program_name=${FUNCNAME[0]}
            echo 'Upload files on Google Drive.'
            echo ''
            echo "Usage: ${program_name} [options...] [<file>]*"
            echo ''
            echo ' -c, --config <file>              Configuration file containing Google Drive credentials'
            echo ' -f, --google-drive-folder <name> Name of the folder where to download the backup. The folder is created if it does not exist'
            return 0
        elif [[ $1 == '-c' || $1 == '--config' ]]
        then
            local config=$2
            shift
        elif [[ $1 == '-f' || $1 == '--google-drive-folder' ]]
        then
            local google_drive_folder_name=$2
            shift
        else
            files+=($1)
        fi
        shift
    done

    retrieve-access-token -c ${config}
    local authorization="Authorization: Bearer ${access_token}"

    # Retrieve the folder on google drive
    local google_drive_folders=()
    if [[ ! -z "${google_drive_folder_name}" ]]
    then
        local google_drive_folder_mime_type='application/vnd.google-apps.folder'
        local google_drive_folder_id=$(curl -s https://www.googleapis.com/drive/v3/files --get \
            --header "${authorization}" \
            --data-urlencode "q=mimeType='${google_drive_folder_mime_type}' and name = '${google_drive_folder_name}' and trashed = false" \
        | jq -r '.files[0].id //empty')

        # Create the folder on google drive if not exists
        if [[ -z "${google_drive_folder_id}" ]]
        then
            echo "Creating folder ${google_drive_folder_name} on Google Drive..."
            google_drive_folder_id=$(curl -s https://www.googleapis.com/drive/v3/files \
                --request POST \
                --header "${authorization}" \
                --header 'Content-Type: application/json' \
                --data "{name: '${google_drive_folder_name}', mimeType: '${google_drive_folder_mime_type}'}" \
            | jq -r '.id')
        fi

        google_drive_folders+=(${google_drive_folder_id})
    fi

    # Upload files
    for file_path in "${files[@]}"
    do
        echo "Uploading file ${file_path}..."
        local file=$(basename ${file_path})
        local metadata=$(jq --null-input \
            '{"name": $name, "parents": $ARGS.positional}' \
            --arg name "${file}" \
            --args -- "${google_drive_folders[@]}")
        curl https://www.googleapis.com/upload/drive/v3/files?uploadType=multipart \
            --request POST \
            --header "${authorization}" \
            --form "metadata=${metadata};type=application/json" \
            --form "file=@${file_path}"
    done
}

function download-files () {
    # Check arguments
    local files=()
    while [ $# -gt 0 ]
    do
        if [[ $1 == "-h" || $1 == "--help" ]]
        then
            local program_name=${FUNCNAME[0]}
            echo 'Downloads files from Google Drive.'
            echo ''
            echo "Usage: ${program_name} [options...] [<file>]*"
            echo ''
            echo ' -c, --config <file>              Configuration file containing Google Drive credentials'
            return 0
        elif [[ $1 == '-c' || $1 == '--config' ]]
        then
            local config=$2
            shift
        else
            files+=($1)
        fi
        shift
    done

    retrieve-access-token -c ${config}
    local authorization="Authorization: Bearer ${access_token}"

    # Download each file
    for google_drive_file_name in "${files[@]}"
    do
        # Retrieve the file ID on google drive
        echo "Retrieving file ${google_drive_file_name}..."
        local google_drive_folder_mime_type='application/vnd.google-apps.folder'
        local google_drive_file_id=$(curl -s https://www.googleapis.com/drive/v3/files --get \
            --header "${authorization}" \
            --data-urlencode "q=mimeType!='${google_drive_folder_mime_type}' and name = '${google_drive_file_name}' and trashed = false" \
        | jq -r '.files[0].id //empty')

        if [[ -z "${google_drive_file_id}" ]]
        then
            echo "File ${google_drive_file_name} not found on Google Drive..."
            continue
        fi

        curl https://www.googleapis.com/drive/v3/files/${google_drive_file_id} --get \
            --header "${authorization}" \
            --data-urlencode "alt=media" \
            -o ${google_drive_file_name}
    done
}
