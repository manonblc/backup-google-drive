# Backup on Google Drive

Bash scripts to create, upload a local backup and download files on/from Google Drive using cURL.

## Dependencies

The upload and download scripts use [`jq`](https://stedolan.github.io/jq/download/) package to communicate with the Google Drive API.

## Create a local backup

To create a local backup, run the following command:

```sh
source backup.sh && create-backup [<file>]*
```

This command will create a `.tar` archive with the current date as its name: `<YYYY-MM-DD>.tar`. This archive will be moved to the `${HOME}` directory. 

## Upload files on Google Drive

To upload one or more files, run the following command:

```sh
source backup.sh && upload-files [<file>]* [--config <file>] [--google-drive-folder <name>]
```

This command needs the OAuth 2.0 credentials through the `GOOGLE_DRIVE_CLIENT_ID` and `GOOGLE_DRIVE_CLIENT_SECRET` variables. They can be environment variables or provided by the configuration file with the `-c` or `--config` argument.

## Download files from Google Drive

To download one or more files, run the following command:

```sh
source backup.sh && download-files [<file>]* [--config <file>]
```

This command needs the OAuth 2.0 credentials through the `GOOGLE_DRIVE_CLIENT_ID` and `GOOGLE_DRIVE_CLIENT_SECRET` variables. They can be environment variables or provided by the configuration file with the `-c` or `--config` argument.

## OAuth 2.0 client configuration

To upload and download the archive, the script uses the [Google Drive API](https://developers.google.com/drive/api/) thanks to the OAuth 2.0 credentials. To create these credentials, you have to use the [Google Cloud Console interface](https://console.cloud.google.com) and:
1. [create a project](https://console.cloud.google.com/projectcreate),
2. activate the [Google Drive API](https://console.cloud.google.com/apis/api/drive.googleapis.com),
3. [configure the OAuth consent screen](https://console.cloud.google.com/apis/credentials/consent) by adding the access level `https://www.googleapis.com/auth/drive.file`,
4. [create an OAuth client ID](https://console.cloud.google.com/apis/credentials/oauthclient) for a desktop application.

## Limitations

At each launch of the script, the command recovers a new token. This token is not saved and is not refreshed.

## Automation

You can add your own `backup` function by reusing the `BACKUP_PATH` variable to fully automate this process:

```sh
function backup() {
    create-backup [<file>]*
    upload-backup ${BACKUP_PATH} [--config <file>] [--google-drive-folder <name>]
}
```
